# Embed Todoist in Notion

Workaround for a bug, in Notion.so you can paste Todoist URL's to embed them, but this will always return you to the Today Page.
This is because the # is forgotten by Notion, so this small script works around that by hiding the # until needed.

Patches always welcome, but let's hope it gets a fix soon

